# palmomedia Version Vue.js
![pipeline](https://gitlab.com/palmomedia/version-vuejs/badges/master/pipeline.svg?style=flat)

[![Gitter](https://badges.gitter.im/palmomedia/community.svg)](https://gitter.im/palmomedia/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

Dies ist der der Versuch mit einem Headless CMS (hier Wordpress als API) eine auf dem JAMStack basierte Webseite zu erschaffen für palmomedia.de
Dabei werden die Drei Frameworks bzw. Bibliotheken VueJS, ReactJS und Angular unter die Lupe genommen.
https://angular.palmomedia.de/blog/eine-webseite-drei-frameworks-jamstack

## CMS
Headless oder nicht: für unsere Zwecke verwenden wir Wordpress.
Login: https://api-blog.palmomedia.de/
Wie sind wir auf die Idee gekommen? Das Tutorial dazu findet ihr hier: https://snipcart.com/blog/wordpress-vue-headless

## Vue.js
Dieses Projekt basiert auf ner Menge ☕ und ein paar schlaflosen 💤 Nächten - bzw. dem HelloWorld Example und viel Google.
Es wird vue-router zum routen verwendet sowie BootstrapVue, Font Awesome und Axios für die API Calls.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Config
Zum stehlen des "Blog Beispiels" braucht ihr nur den API Endpoint in ./config auszutauschen.
Dann habt ihr quasi eure Wordpressseite angebunden. 
Ich habe in meinem Beispiel *nicht* die von WP angebotenen Endpoints für posts und pages verwendet (https://api-blog.palmomedia.de/wp-json/wp/v2/posts) da diese mir zu instabil vorkamen. Daher sind meine Endpoints selfmade.

### Get all post
https://api-blog.palmomedia.de/wp-json/content/v1/post

### Get sitemap
https://api-blog.palmomedia.de/wp-json/content/v1/sitemap

### Get a specific post
https://api-blog.palmomedia.de/wp-json/content/v1/post/xyz

### Get a specific page
https://api-blog.palmomedia.de/wp-json/content/v1/page/home
