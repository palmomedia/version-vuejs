/**
 * @type {import('vue-router').RouteConfig[]}
 */

var paths = [
    {
        path: '/',
        name: 'home',
    },
    {
        path: '/blog',
        name: 'blog',
    },
    {
        path: '/blog/:post',
        name: 'blogpost',
    },
    {
        path: '/blog/impressum',
        name: 'impressum',
    },
    {
        path: '/*',
        name: '404',
    }
  ]
  
  module.exports = { default: paths } // ES6 Module Interoperability