import Vue from 'vue'
import Router from 'vue-router'
//import { paths } from './router.paths'

/*import Blog from '@/components/Blog.vue'
import BlogPost from '@/components/BlogPost.vue'
import Page from '@/components/Page.vue'
import Vier from '@/components/404.vue'*/

Vue.use(Router)

function lazyLoad(view){
  return() => import(`@/components/${view}.vue`)
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
        path: '/',
        name: 'home',
        component: lazyLoad('Page')
    },
    {
        path: '/blog',
        name: 'test2',
        component: lazyLoad('Blog')
    },
    {
        path: '/blog/:post',
        name: 'blogpost',
        component: lazyLoad('BlogPost')
    },
    {
        path: '/404',
        name: '404-static',
        component: lazyLoad('Vier')
    },
    {
        path: '/:page',
        name: 'page',
        component: lazyLoad('Page')
    },
    {
        path: '/*',
        name: '404',
        component: lazyLoad('Vier')
    }
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
})