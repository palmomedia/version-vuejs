const SitemapPlugin = require('sitemap-webpack-plugin').default
const routerPaths = require('./src/router.paths').default.map(r => r.path).filter(p => !p.match(/\*/))

module.exports = {
    configureWebpack: {
        devtool: 'source-map',
        plugins: [
            new SitemapPlugin('https://www.palmomedia.de', routerPaths, {
            fileName: 'sitemap.xml',
            lastMod: true,
            changeFreq: 'monthly'
            })
        ],
        /*optimization: {
            splitChunks: {
            minSize: 10000,
            maxSize: 250000,
            }
        }*/
    }
}